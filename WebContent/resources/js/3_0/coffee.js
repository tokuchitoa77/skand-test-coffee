$(document).ready(function() {
	$('.coffee input[type="text"]').each(function() {
		$(this).on('focus', function() {
			if ($(this).val() == '0') {
				$(this).val('');
			}
			$(this).removeClass('input-error');
		})

		$(this).on('blur', function() {
			if ($(this).val() == '') {
				$(this).val('0');
			}
		})

		$(this).on('input', function() {
			quantityOnChangeFunction($(this));
		})

		if ($(this).val() != '0') {
			// if validation failed css-class wasn't saved
			$(this).closest('.coffee').addClass('coffee-selected');
		}
	});

	$('.coffee-more').each(function() {
		$(this).on('click', function() {
			var quantityInput = $(this).closest('tr').find('.coffee-quantity');
			changeCoffeeQuantity(quantityInput, 1);
		});
	});

	$('.coffee-less').each(function() {
		$(this).on('click', function() {
			var quantityInput = $(this).closest('tr').find('.coffee-quantity');
			changeCoffeeQuantity(quantityInput, -1);
		});
	});
});

/**
 * To change quantity when clicked on 'coffee-less / coffee-more' control.
 */
function changeCoffeeQuantity(quantityInput, amount) {
	var val = parseInt(quantityInput.val());
	val += amount;
	quantityInput.val(val);
	quantityOnChangeFunction(quantityInput);
}

/**
 * To process quantity input value changes and mark coffee as selected /
 * deselected.
 */
var quantityOnChangeFunction = function(quantityInput) {
	var val = parseInt(quantityInput.val()); // '12qwerty' -> '12'
	if (val < 0 || isNaN(val)) {
		val = 0;
	}
	// only integer will remain in input
	quantityInput.val(val);

	if (val == 0) {
		quantityInput.closest('.coffee').removeClass('coffee-selected');
	} else {
		quantityInput.closest('.coffee').addClass('coffee-selected');
	}
}
