CREATE TABLE public.coffee_type
(
    id serial NOT NULL,
    name character varying(200) NOT NULL,
    price numeric(10,2) NOT NULL,
    disabled "char" NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE UNIQUE INDEX ct_i
    ON public.coffee_type USING btree
    (id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE public.coffee_order
(
    id serial NOT NULL,
    date timestamp without time zone NOT NULL,
    name character varying(100),
    delivery_address character varying(200) NOT NULL,
    cost numeric(10,2) NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE UNIQUE INDEX co_i
    ON public.coffee_order USING btree
    (id ASC NULLS LAST)
    TABLESPACE pg_default;
	
CREATE TABLE public.coffee_order_item
(
    id serial NOT NULL,
    type_id integer NOT NULL,
    order_id integer NOT NULL,
    quantity integer NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE UNIQUE INDEX coi_i
    ON public.coffee_order_item USING btree
    (id ASC NULLS LAST)
    TABLESPACE pg_default;
	
CREATE INDEX coi_i2
    ON public.coffee_order_item USING btree
    (order_id ASC NULLS LAST)
    TABLESPACE pg_default;
	
CREATE INDEX coi_i3
    ON public.coffee_order_item USING btree
    (type_id ASC NULLS LAST)
    TABLESPACE pg_default;
	
ALTER TABLE public.coffee_order_item
    ADD CONSTRAINT coi_co FOREIGN KEY (order_id)
    REFERENCES public.coffee_order (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
	
ALTER TABLE public.coffee_order_item
    ADD CONSTRAINT coi_ct FOREIGN KEY (type_id)
    REFERENCES public.coffee_type (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
	
CREATE TABLE public.configuration
(
    id character varying(20) NOT NULL,
    value character varying(30),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

INSERT INTO coffee_type(name,price,disabled) VALUES
('Americano', 1, 'N'),
('Babycino', 2, 'N'),
('Breve', 3, 'N'),
('Caffe'' Freddo', 4, 'N'),
('Caffe Latte', 5, 'N'),
('Caffe Mocha', 6, 'N'),
('Cappuccino Chiaro', 7, 'N'),
('Cappuccino Scuro', 8, 'N'),
('Cappuccino', 9, 'N'),
('Con panna', 10, 'N'),
('Cortado', 11, 'N'),
('Corretto', 12, 'N'),
('Doppio', 13, 'N'),
('Espresso con Panna', 14, 'N'),
('Espresso Lungo', 15, 'N'),
('Espresso Romano', 16, 'N'),
('Flat White', 17, 'N'),
('Hammerhead', 18, 'N'),
('Irish Coffee', 19, 'N'),
('Bad Coffee', 666, 'Y');

INSERT INTO configuration(id,value) VALUES 
('n', '5'),
('m', '2.00'),
('x', '10.00'),
('maxCupsPerType', '50');