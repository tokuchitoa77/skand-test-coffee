<h3>MVC - JSF</h3>

<h3>Server - Apache Tomcat 9</h3>
 example config: <br />
  /serverConfig/Coffee.xml -> [server_home]/conf/Catalina/[host]/<br />
  /serverConfig/Coffee.hibernate.properties -> any place referenced in Coffee.xml

<h3>Test DB - PostgreSQL 9.6</h3>
init script: /sql/postgreSQL.sql

<h3>Persistence - Hibernate / JDBC</h3>
 controlled by environment variable in Coffee.xml ('hibernate' / 'jdbc')
 
<h3>Build - Apache Maven 3.5.0</h3>
 mvn package

<h3>Deploy</h3>
 /target/Coffee.war -> [server_home]/webapps/<br />
 http://[host]:[port]/Coffee
