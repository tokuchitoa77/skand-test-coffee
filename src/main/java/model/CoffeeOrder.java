package model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "coffee_order")
public class CoffeeOrder {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@OneToMany(mappedBy = "coffeeOrder", cascade=CascadeType.ALL)
	private List<CoffeeOrderItem> orderItems;

	@Column(name = "name")
	private String payerName;

	@Column(name = "delivery_address")
	private String deliveryAddress;

	@Column(name = "cost")
	private BigDecimal totalCost;

	@Column(name = "date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderDate;

	@Transient
	private BigDecimal subtotalCost;

	@Transient
	private BigDecimal deliveryCost;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<CoffeeOrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<CoffeeOrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public BigDecimal getSubtotalCost() {
		return subtotalCost;
	}

	public void setSubtotalCost(BigDecimal subtotalCost) {
		this.subtotalCost = subtotalCost;
	}

	public BigDecimal getDeliveryCost() {
		return deliveryCost;
	}

	public void setDeliveryCost(BigDecimal deliveryCost) {
		this.deliveryCost = deliveryCost;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

}
