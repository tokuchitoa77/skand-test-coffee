package model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "coffee_order_item")
public class CoffeeOrderItem {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "type_id")
	private CoffeeType coffeeType;

	@ManyToOne
	@JoinColumn(name = "order_id")
	private CoffeeOrder coffeeOrder;

	@Column(name = "quantity")
	private int quantity;

	@Transient
	private int freeQuantity;

	public CoffeeOrderItem() {
	}

	public CoffeeOrderItem(CoffeeType coffeeType, CoffeeOrder coffeeOrder) {
		this.coffeeType = coffeeType;
		this.coffeeOrder = coffeeOrder;
	}

	public BigDecimal getCost() {
		return coffeeType.getPrice().multiply(BigDecimal.valueOf(quantity - freeQuantity));
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CoffeeType getCoffeeType() {
		return coffeeType;
	}

	public void setCoffeeType(CoffeeType coffeeType) {
		this.coffeeType = coffeeType;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public CoffeeOrder getCoffeeOrder() {
		return coffeeOrder;
	}

	public void setCoffeeOrder(CoffeeOrder coffeeOrder) {
		this.coffeeOrder = coffeeOrder;
	}

	public int getFreeQuantity() {
		return freeQuantity;
	}

	public void setFreeQuantity(int freeQuantity) {
		this.freeQuantity = freeQuantity;
	}

}
