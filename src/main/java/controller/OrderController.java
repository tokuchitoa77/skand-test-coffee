package controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import context.ResourceBundleUTF8;
import model.CoffeeOrder;
import model.CoffeeOrderItem;
import service.CoffeeOrderService;
import service.exception.CoffeeOrderServiceException;

@SessionScoped
@ManagedBean
public class OrderController {

	@ManagedProperty(value = "#{configController}")
	private ConfigController configController;

	private CoffeeOrder order;
	private CoffeeOrderService coffeeOrderService = new CoffeeOrderService();

	/**
	 * To create a new order on first access or when a previous order was
	 * confirmed.
	 */
	public void createNewOrderIfNeeded() {
		if (order == null) {
			order = coffeeOrderService.createNewOrder();
		}
	}

	/**
	 * To prepare an order for confirmation.
	 */
	public String makeOrder() {
		if (getOrderItemsWithNotZeroQuantity().isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(ResourceBundleUTF8.getUTF8Bundle().getString("order_empty")));
			return "coffee";
		}

		coffeeOrderService.prepareOrder(order,
				configController.getFreeCupParameter(),
				configController.getDeliveryCostParameter(),
				configController.getFreeDeliveryParameter());

		return "order";
	}

	public String confirmOrder() {
		try {
			coffeeOrderService.confirmOrder(order);
		} catch (CoffeeOrderServiceException e) {
			e.printStackTrace();
			return "error";
		} finally {
			order = null;
		}
		return "orderResult";
	}

	public List<CoffeeOrderItem> getOrderItemsWithNotZeroQuantity() {
		return order.getOrderItems().stream()
				.filter(item -> item.getQuantity() != 0)
				.collect(Collectors.toList());
	}

	public CoffeeOrder getOrder() {
		return order;
	}

	public void setOrder(CoffeeOrder order) {
		this.order = order;
	}

	public void setConfigController(ConfigController configController) {
		this.configController = configController;
	}

}
