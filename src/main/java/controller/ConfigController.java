package controller;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import model.ConfigParameter;
import service.ConfigService;

@ApplicationScoped
@ManagedBean
public class ConfigController {
	
	private ConfigService configService = new ConfigService();

	public ConfigParameter getFreeCupParameter() {
		return configService.getConfigParameter("n");
	}

	public ConfigParameter getDeliveryCostParameter() {
		return configService.getConfigParameter("m");
	}

	public ConfigParameter getFreeDeliveryParameter() {
		return configService.getConfigParameter("x");
	}

	public ConfigParameter getMaxCupsPerTypeParameter() {
		return configService.getConfigParameter("maxCupsPerType");
	}

}
