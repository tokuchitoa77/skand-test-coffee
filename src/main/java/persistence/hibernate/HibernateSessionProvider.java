package persistence.hibernate;

import java.io.File;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import model.CoffeeOrder;
import model.CoffeeOrderItem;
import model.CoffeeType;
import model.ConfigParameter;

public class HibernateSessionProvider {

	private static SessionFactory sessionFactory;
	private static StandardServiceRegistry registry;

	public static void init(InitialContext context) throws NamingException {
		File hibernatePropertiesFile = new File((String) context.lookup("java:/comp/env/hibernateProperties"));

		registry = new StandardServiceRegistryBuilder()
				.loadProperties(hibernatePropertiesFile)
				.build();

		sessionFactory = new MetadataSources(registry)
				.addAnnotatedClass(CoffeeOrder.class)
				.addAnnotatedClass(CoffeeOrderItem.class)
				.addAnnotatedClass(CoffeeType.class)
				.addAnnotatedClass(ConfigParameter.class)
				.buildMetadata()
				.buildSessionFactory();
	}

	public static void closeSessionFactory() {
		if (sessionFactory != null) {
			sessionFactory.close();
		}
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}

	public static Session getSession() {
		return sessionFactory.openSession();
	}

}
