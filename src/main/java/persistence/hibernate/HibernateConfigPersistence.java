package persistence.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.ConfigParameter;
import persistence.api.ConfigPersistence;

public class HibernateConfigPersistence implements ConfigPersistence {

	@Override
	public ConfigParameter getConfigParameter(String id) {
		ConfigParameter result = null;

		Session session = HibernateSessionProvider.getSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();
			result = session.find(ConfigParameter.class, id);
			transaction.commit();
		} catch (Exception e) {
			if (transaction.isActive()) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

}
