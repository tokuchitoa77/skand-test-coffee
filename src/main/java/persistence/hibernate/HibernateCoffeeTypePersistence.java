package persistence.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.CoffeeType;
import persistence.api.CoffeeTypePersistence;

public class HibernateCoffeeTypePersistence implements CoffeeTypePersistence {

	@Override
	public List<CoffeeType> getEnabledCoffeeTypes() {

		List<CoffeeType> result = new ArrayList<>();

		Session session = HibernateSessionProvider.getSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();
			result = session.createQuery("from CoffeeType where disabled != 'Y'", CoffeeType.class).getResultList();
			transaction.commit();
		} catch (Exception e) {
			if (transaction.isActive()) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

}
