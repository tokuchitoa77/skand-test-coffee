package persistence.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.CoffeeOrder;
import persistence.api.CoffeeOrderPersistence;
import persistence.exception.CoffeePersistenceException;

public class HibernateCoffeeOrderPersistence implements CoffeeOrderPersistence {

	@Override
	public void persist(CoffeeOrder order) throws CoffeePersistenceException {
		Session session = HibernateSessionProvider.getSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();
			session.persist(order);
			transaction.commit();
		} catch (Exception e) {
			if (transaction.isActive()) {
				transaction.rollback();
			}
			e.printStackTrace();
			throw new CoffeePersistenceException("couldn't persist order", e);
		} finally {
			session.close();
		}
	}

}
