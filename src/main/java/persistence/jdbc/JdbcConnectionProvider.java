package persistence.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class JdbcConnectionProvider {

	private static DataSource datasource;

	public static void init(InitialContext context) throws NamingException {
		datasource = (DataSource) context.lookup("java:/comp/env/jdbc/coffee");
	}

	public static Connection getConnection() throws SQLException {
		return datasource.getConnection();
	}

}
