package persistence.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.ConfigParameter;
import persistence.api.ConfigPersistence;

public class JdbcConfigPersistence implements ConfigPersistence {

	private static final String selectParameterQuery = "select * from configuration where id = ?";

	@Override
	public ConfigParameter getConfigParameter(String id) {
		ConfigParameter result = null;

		try (Connection con = JdbcConnectionProvider.getConnection();
				PreparedStatement st = con.prepareStatement(selectParameterQuery);) {
			st.setString(1, id);

			try (ResultSet rs = st.executeQuery()) {
				if (rs.next()) {
					result = new ConfigParameter();
					result.setId(rs.getString("id"));
					result.setValue(rs.getString("value"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
