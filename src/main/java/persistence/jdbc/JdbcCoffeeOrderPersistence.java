package persistence.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import model.CoffeeOrder;
import model.CoffeeOrderItem;
import persistence.api.CoffeeOrderPersistence;
import persistence.exception.CoffeePersistenceException;

public class JdbcCoffeeOrderPersistence implements CoffeeOrderPersistence {

	private static final String insertOrderQuery = "insert into coffee_order(date, name, delivery_address, cost) values(?,?,?,?)";
	private static final String insertOrderItemQuery = "insert into coffee_order_item(type_id, order_id, quantity) values(?,?,?)";

	@Override
	public void persist(CoffeeOrder order) throws CoffeePersistenceException {
		try (Connection con = JdbcConnectionProvider.getConnection()) {

			con.setAutoCommit(false);

			try (PreparedStatement stOrder = con.prepareStatement(insertOrderQuery, Statement.RETURN_GENERATED_KEYS)) {
				stOrder.setTimestamp(1, new Timestamp(order.getOrderDate().getTime()));
				stOrder.setString(2, order.getPayerName());
				stOrder.setString(3, order.getDeliveryAddress());
				stOrder.setBigDecimal(4, order.getTotalCost());
				stOrder.executeUpdate();
				
				int insertedOrderId = -1;

				try (ResultSet generatedKeys = stOrder.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						insertedOrderId = generatedKeys.getInt(1);
					} else {
						throw new CoffeePersistenceException(
								"insert into coffee_order failed (wasn't able to obtain generated id)");
					}
				}

				try (PreparedStatement stOrderItem = con.prepareStatement(insertOrderItemQuery)) {
					for (CoffeeOrderItem item : order.getOrderItems()) {
						stOrderItem.setInt(1, item.getCoffeeType().getId());
						stOrderItem.setInt(2, insertedOrderId);
						stOrderItem.setInt(3, item.getQuantity());
						stOrderItem.addBatch();
					}
					stOrderItem.executeBatch();
				}

				con.commit();
			} catch (SQLException e) {
				e.printStackTrace();
				con.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
