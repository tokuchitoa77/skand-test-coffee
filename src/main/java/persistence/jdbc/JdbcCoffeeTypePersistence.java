package persistence.jdbc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.CoffeeType;
import persistence.api.CoffeeTypePersistence;

public class JdbcCoffeeTypePersistence implements CoffeeTypePersistence {

	@Override
	public List<CoffeeType> getEnabledCoffeeTypes() {
		List<CoffeeType> result = new ArrayList<>();

		try (Connection con = JdbcConnectionProvider.getConnection();
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("select * from coffee_type")) {
			while (rs.next()) {
				CoffeeType coffeeType = new CoffeeType();
				coffeeType.setId(rs.getInt("id"));
				coffeeType.setName(rs.getString("name"));
				coffeeType.setPrice(new BigDecimal(rs.getString("price")));
				coffeeType.setDisabled(rs.getString("disabled"));
				result.add(coffeeType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
