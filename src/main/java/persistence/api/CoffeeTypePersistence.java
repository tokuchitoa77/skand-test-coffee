package persistence.api;

import java.util.List;

import model.CoffeeType;

public interface CoffeeTypePersistence {

	/**
	 * 
	 * @return a list of {@link CoffeeType} instances corresponding to all
	 *         enabled coffee types or an empty list if no enabled types found
	 */
	public List<CoffeeType> getEnabledCoffeeTypes();

}
