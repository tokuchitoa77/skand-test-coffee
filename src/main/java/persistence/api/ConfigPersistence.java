package persistence.api;

import model.ConfigParameter;

public interface ConfigPersistence {

	/**
	 * Finds configuration parameter by id.
	 * @param id
	 * @return configuration parameter or null if was not able to find
	 */
	ConfigParameter getConfigParameter(String id);

}
