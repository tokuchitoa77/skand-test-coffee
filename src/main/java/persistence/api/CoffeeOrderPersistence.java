package persistence.api;

import model.CoffeeOrder;
import persistence.exception.CoffeePersistenceException;

public interface CoffeeOrderPersistence {

	/**
	 * Tries to persist a passed {@link CoffeeOrder} instance.
	 * @param order
	 * @throws CoffeePersistenceException if was not able to persist
	 */
	public void persist(CoffeeOrder order) throws CoffeePersistenceException;

}
