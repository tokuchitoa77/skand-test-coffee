package persistence.exception;

public class CoffeePersistenceException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CoffeePersistenceException(String message) {
		super(message);
	}
	
	public CoffeePersistenceException(Throwable cause) {
		super(cause);
	}

	public CoffeePersistenceException(String message, Throwable cause) {
		super(message, cause);
	}
}
