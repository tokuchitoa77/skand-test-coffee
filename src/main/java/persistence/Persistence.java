package persistence;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import persistence.api.CoffeeOrderPersistence;
import persistence.api.CoffeeTypePersistence;
import persistence.api.ConfigPersistence;
import persistence.exception.CoffeePersistenceException;
import persistence.hibernate.HibernateCoffeeOrderPersistence;
import persistence.hibernate.HibernateCoffeeTypePersistence;
import persistence.hibernate.HibernateConfigPersistence;
import persistence.hibernate.HibernateSessionProvider;
import persistence.jdbc.JdbcCoffeeOrderPersistence;
import persistence.jdbc.JdbcCoffeeTypePersistence;
import persistence.jdbc.JdbcConfigPersistence;
import persistence.jdbc.JdbcConnectionProvider;

/**
 * A class to initialize/cleanUp persistence layer classes and to pass
 * persistence instances to services.
 *
 */
public class Persistence {

	private static final String HIBERNATE_PERSISTENCE = "hibernate";
	private static final String JDBC_PERSISTENCE = "jdbc";

	private static String persistenceType;

	private static CoffeeOrderPersistence coffeeOrderPersistence;
	private static CoffeeTypePersistence coffeeTypePersistence;
	private static ConfigPersistence configPersistence;

	/**
	 * Tries to initialize persistence layer classes.
	 * 
	 * @throws CoffeePersistenceException
	 *             if couldn't recognize persistence type provided by
	 *             configuration
	 */
	public static void init() throws CoffeePersistenceException {
		InitialContext context;
		try {
			context = new InitialContext();
			persistenceType = (String) context.lookup("java:/comp/env/persistenceType");

			if (HIBERNATE_PERSISTENCE.equals(persistenceType)) {
				HibernateSessionProvider.init(context);
				coffeeOrderPersistence = new HibernateCoffeeOrderPersistence();
				coffeeTypePersistence = new HibernateCoffeeTypePersistence();
				configPersistence = new HibernateConfigPersistence();
			} else if (JDBC_PERSISTENCE.equals(persistenceType)) {
				JdbcConnectionProvider.init(context);
				coffeeOrderPersistence = new JdbcCoffeeOrderPersistence();
				coffeeTypePersistence = new JdbcCoffeeTypePersistence();
				configPersistence = new JdbcConfigPersistence();
			} else {
				throw new CoffeePersistenceException(
						String.format("persistence type [%s] not supported", persistenceType));
			}
		} catch (NamingException e) {
			e.printStackTrace();
			throw new CoffeePersistenceException(e);
		}
	}

	/**
	 * Tries to free resources of initialized persistence layer.
	 */
	public static void cleanUp() {
		if (HIBERNATE_PERSISTENCE.equals(persistenceType)) {
			HibernateSessionProvider.closeSessionFactory();
		}
	}

	public static CoffeeOrderPersistence getCoffeeOrderPersistence() {
		return coffeeOrderPersistence;
	}

	public static CoffeeTypePersistence getCoffeeTypePersistence() {
		return coffeeTypePersistence;
	}

	public static ConfigPersistence getConfigPersistence() {
		return configPersistence;
	}

}
