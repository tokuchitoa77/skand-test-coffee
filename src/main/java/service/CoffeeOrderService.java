package service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import model.CoffeeOrder;
import model.CoffeeOrderItem;
import model.ConfigParameter;
import persistence.Persistence;
import persistence.api.CoffeeOrderPersistence;
import persistence.exception.CoffeePersistenceException;
import service.exception.CoffeeOrderServiceException;

public class CoffeeOrderService {

	private CoffeeTypeService coffeeTypeService = new CoffeeTypeService();
	private CoffeeOrderPersistence orderPersistence = Persistence.getCoffeeOrderPersistence();

	/**
	 * Creates a new {@link CoffeeOrder} instance and fills it's
	 * {@link CoffeeOrderItem} list with new instances of
	 * {@link CoffeeOrderItem} for each enabled {@link CoffeeType}.
	 * 
	 * @return created instance
	 */
	public CoffeeOrder createNewOrder() {
		CoffeeOrder order = new CoffeeOrder();

		List<CoffeeOrderItem> orderItems = coffeeTypeService.getEnabledCoffeeTypes().stream()
				.map(coffeeType -> new CoffeeOrderItem(coffeeType, order))
				.collect(Collectors.toList());

		order.setOrderItems(orderItems);

		return order;
	}

	/**
	 * For a passed {@link CoffeeOrder} parameter: <br/>
	 * - calculates and sets freeQuantity of each {@link CoffeeOrderItem} with
	 * quantity > 0 <br/>
	 * - calculates and sets subtotalCost, deliveryCost and totalCost.
	 * 
	 * @param order
	 * @param freeCupParameter
	 * @param deliveryCostParameter
	 * @param freeDeliveryParameter
	 */
	public void prepareOrder(CoffeeOrder order,
			ConfigParameter freeCupParameter, ConfigParameter deliveryCostParameter,
			ConfigParameter freeDeliveryParameter) {
		BigDecimal subtotal = BigDecimal.ZERO;
		BigDecimal deliveryCostParameterValue = new BigDecimal(deliveryCostParameter.getValue());
		BigDecimal freeDeliveryParameterValue = new BigDecimal(freeDeliveryParameter.getValue());

		for (CoffeeOrderItem item : order.getOrderItems()) {
			if (item.getQuantity() != 0) {
				item.setFreeQuantity(item.getQuantity() / freeCupParameter.integerValue());
				subtotal = subtotal.add(item.getCost());
			}
		}

		order.setSubtotalCost(subtotal);

		if (subtotal.compareTo(freeDeliveryParameterValue) > 0) {
			order.setDeliveryCost(BigDecimal.ZERO);
		} else {
			order.setDeliveryCost(deliveryCostParameterValue);
		}

		order.setTotalCost(subtotal.add(order.getDeliveryCost()));
	}

	/***
	 * For a passed {@link CoffeeOrder} parameter: <br/>
	 * - sets order date to current date<br/>
	 * - removes order items with zero quantity<br/>
	 * - validates order<br/>
	 * - persists order<br/>
	 * 
	 * @param order
	 * @throws CoffeeOrderServiceException
	 */
	public void confirmOrder(CoffeeOrder order) throws CoffeeOrderServiceException {
		order.getOrderItems().removeIf(item -> item.getQuantity() == 0);
		order.setOrderDate(new Date());

		validateOrder(order);

		try {
			orderPersistence.persist(order);
		} catch (CoffeePersistenceException e) {
			e.printStackTrace();
			throw new CoffeeOrderServiceException(e);
		}
	}

	private void validateOrder(CoffeeOrder order) throws CoffeeOrderServiceException {
		if (order.getDeliveryAddress() == null || "".equals(order.getDeliveryAddress())) {
			throw new CoffeeOrderServiceException("order address is null or empty");
		}
		if (order.getOrderItems().stream().anyMatch(item -> item.getQuantity() < 0)) {
			throw new CoffeeOrderServiceException("order has an item with illegal quantity");
		}
		if (BigDecimal.ZERO.compareTo(order.getTotalCost()) >= 0) {
			throw new CoffeeOrderServiceException("order cost not positive");
		}
	}

}
