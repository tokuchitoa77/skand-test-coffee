package service.exception;

public class CoffeeOrderServiceException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CoffeeOrderServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public CoffeeOrderServiceException(String message) {
		super(message);
	}

	public CoffeeOrderServiceException(Throwable cause) {
		super(cause);
	}
	
}
