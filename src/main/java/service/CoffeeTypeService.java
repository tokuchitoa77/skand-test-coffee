package service;

import java.util.List;

import model.CoffeeType;
import persistence.Persistence;
import persistence.api.CoffeeTypePersistence;

public class CoffeeTypeService {

	private CoffeeTypePersistence coffeTypePersistence = Persistence.getCoffeeTypePersistence();

	/**
	 * 
	 * @return a list of {@link CoffeeType} instances corresponding to all enabled coffee types
	 */
	public List<CoffeeType> getEnabledCoffeeTypes() {
		return coffeTypePersistence.getEnabledCoffeeTypes();
	}

}
