package service;

import model.ConfigParameter;
import persistence.Persistence;
import persistence.api.ConfigPersistence;

public class ConfigService {

	private ConfigPersistence configPersistence = Persistence.getConfigPersistence();

	/**
	 * Finds configuration parameter by id.
	 * @param id
	 * @return configuration parameter or null if was not able to find
	 */
	public ConfigParameter getConfigParameter(String id) {
		return configPersistence.getConfigParameter(id);
	}

}
